# Verkehrsampel

## Designkurzbeschreibung 

- 4 Seiten Ampel
- 1 Taster f�r An/Aus (WakeUp/Sleep) und Mode (Normal/Nacht)
- LEDs �ber Transistor angesteuert
- Interner Taktgeber

## Benutzung

Wenn Aus
* -> Taster dr�cken -> An

Wenn An 
* -> Taster kurz dr�cken -> Wechsel zwischen Tag/Nacht Modus
* -> Taster lang dr�cken -> Aus
 
## Verwendung 
- Ampel f�r Alma

 
# Einfache CQM Ampeln

## Variante f�r fhc und ful

### Ampelphasen

- rot
- rot + gelb
- gelb
- gelb + gr�n
- gr�n
- (goto 1)

# Bilder von verschiedenen Ampeln

![CQM Ampel 1](https://bytebucket.org/thomo/ampeln/raw/default/images/cqm_ampel_1.jpg)
![CQM Ampel 2](https://bytebucket.org/thomo/ampeln/raw/default/images/cqm_ampel_2.jpg)
![TDD Ampel (red-green-refactor)](https://bytebucket.org/thomo/ampeln/raw/default/images/tdd_ampel.jpg)